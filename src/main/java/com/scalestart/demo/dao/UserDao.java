package com.scalestart.demo.dao;

import com.scalestart.demo.domain.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by sproddaa on 5/22/17.
 */
public interface UserDao extends CrudRepository<User,Long> {
    User findByUsername(String username);
    User findByEmail(String email);
}
