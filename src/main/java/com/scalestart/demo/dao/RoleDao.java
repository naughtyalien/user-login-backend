package com.scalestart.demo.dao;

import com.scalestart.demo.domain.security.Role;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by sproddaa on 5/23/17.
 */
public interface RoleDao extends CrudRepository<Role, Integer> {
    Role findByName(String name);
}
