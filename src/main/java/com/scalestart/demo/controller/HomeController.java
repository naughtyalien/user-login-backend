package com.scalestart.demo.controller;

import com.scalestart.demo.dao.RoleDao;
import com.scalestart.demo.domain.User;
import com.scalestart.demo.domain.security.UserRole;
import com.scalestart.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by sproddaa on 5/18/17.
 */
@Controller
public class HomeController {
    @Autowired
    private UserService userService;

    @Autowired
    private RoleDao roleDao;

    @RequestMapping("/")
    public String home(){
        return "redirect:/index";
    }

    @RequestMapping("/index")
    public String index(){
        return "index";
    }

    @RequestMapping("/userFront")
    public String userfront(Principal principal, Model model){
        User user  = userService.findByUsername(principal.getName());
        return "userfront";
    }

    @RequestMapping(value = "/signup",method = RequestMethod.GET)
    public String signup(Model model){
        User user  = new User();

        model.addAttribute("user",user);
        return "/signup";
    }
    @RequestMapping(value = "/signup",method = RequestMethod.POST)
    public String signup(@ModelAttribute("user") User user,Model model){
        if(userService.checkIfUserExists(user.getUsername(),user.getEmail())){
            if(userService.checkIfUserNameExists(user.getUsername())){
                model.addAttribute("usernameExists",true);
            }
            if(userService.checkIfUserEmailExists((user.getEmail()))){
                model.addAttribute("emailExists",true);
            }
            return "/signup";
        }else{
            Set<UserRole> userRoles = new HashSet<>();
            userRoles.add(new UserRole(user,roleDao.findByName("ROLE_USER")));
            userService.createUser(user,userRoles);
            return "redirect:/";
        }
    }
}
