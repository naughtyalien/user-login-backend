package com.scalestart.demo.controller;

import com.scalestart.demo.domain.User;
import com.scalestart.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

/**
 * Created by sproddaa on 5/23/17.
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/profile",method = RequestMethod.GET)
    private String profile(Principal principal, Model model){
        User user  = userService.findByUsername(principal.getName());
        model.addAttribute("user",user);
        return "profile";
    }

    @RequestMapping(value = "/profile",method = RequestMethod.POST)
    private String profilePost(@ModelAttribute("user") User newUser, Model model){
        User user = userService.findByUsername(newUser.getUsername());
        user.setUsername(newUser.getUsername());
        user.setfName(newUser.getfName());
        user.setlName(newUser.getlName());
        user.setPhone(newUser.getPhone());
        user.setEmail(newUser.getEmail());
        model.addAttribute("user",user);
        userService.saveUser(user);
        return "profile";
    }
}
