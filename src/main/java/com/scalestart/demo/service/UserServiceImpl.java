package com.scalestart.demo.service;

import com.scalestart.demo.dao.RoleDao;
import com.scalestart.demo.dao.UserDao;
import com.scalestart.demo.domain.User;
import com.scalestart.demo.domain.security.UserRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * Created by sproddaa on 5/22/17.
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public void saveUser(User user){
        userDao.save(user);
    }

    public User findByUsername(String username){
        return userDao.findByUsername(username);
    }

    public User findByEmail(String email){
        return userDao.findByEmail(email);
    }

    public boolean checkIfUserExists(String username,String email){
        if(checkIfUserEmailExists(username) || checkIfUserEmailExists(email)){
            return true;
        }
        return false;
    }

    public boolean checkIfUserNameExists(String username){
        if(null != findByUsername(username)){
            return true;
        }
        return false;
    }

    public boolean checkIfUserEmailExists(String email){
        if(null != findByEmail(email)){
            return true;
        }
        return false;
    }

    @Transactional
    public User createUser(User user, Set<UserRole> userRoles) {
        User localUser = userDao.findByUsername(user.getUsername());
        if(localUser!=null){
            LOG.info("User {} already found",user.getUsername());
        }else{
            String encryptedPassword = passwordEncoder.encode(user.getPassword());
            user.setPassword(encryptedPassword);
            for(UserRole ur: userRoles){
                roleDao.save(ur.getRole());
            }

            user.getUserRoles().addAll(userRoles);

            localUser = userDao.save(user);

        }

        return localUser;
    }


    public List<User> findUserList(){
        return (List<User>) userDao.findAll();
    }

    public void enableUser(String username){
        User user = userDao.findByUsername(username);
        user.setEnabled(true);
        userDao.save(user);
    }

    public void disableUser(String username){
        User user = userDao.findByUsername(username);
        user.setEnabled(false);
        userDao.save(user);
    }
}
