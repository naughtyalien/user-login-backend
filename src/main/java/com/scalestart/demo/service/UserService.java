package com.scalestart.demo.service;

import com.scalestart.demo.domain.User;
import com.scalestart.demo.domain.security.UserRole;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;
import java.util.Set;

/**
 * Created by sproddaa on 5/22/17.
 */
public interface UserService {
    void  saveUser(User user);

    User findByUsername(String username);

    User findByEmail(String email);

    boolean checkIfUserExists(String username,String email);

    boolean checkIfUserNameExists(String username);

    boolean checkIfUserEmailExists(String email);

    User createUser(User user, Set<UserRole> userRoles);

    @PreAuthorize("hasRole('ADMIN')")
    List<User> findUserList();

    @PreAuthorize("hasRole('ADMIN')")
    void enableUser(String username);

    @PreAuthorize("hasRole('ADMIN')")
    void disableUser(String username);


}
